class Mutations::CreateBasket < Mutations::BaseMutation
  argument :attributes, Types::BasketAttributes, required: true

  field :basket, Types::BasketType, null: true
  field :errors, [String], null: false

  def resolve(attributes:)
    basket = Basket.where(product_id: attributes[:product_id], customer_id: attributes[:customer_id]).first_or_initialize
    basket.count = attributes[:count]
    if basket.save
      {
          basket: basket,
          errors: []
      }
    else
      {
          basket: nil,
          errors: basket.errors.full_messages
      }
    end
  end
end