class Mutations::CreateProduct < Mutations::BaseMutation
  argument :attributes, Types::ProductAttributes, required: true

  field :product, Types::ProductType, null: true
  field :errors, [String], null: false

  def resolve(attributes:)
    product = Product.new(attributes.to_hash)
    if product.save
      {
          product: product,
          errors: []
      }
    else
      {
          product: nil,
          errors: product.errors.full_messages
      }
    end
  end
end
