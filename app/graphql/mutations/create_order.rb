class Mutations::CreateOrder < Mutations::BaseMutation
  argument :attributes, Types::OrderAttributes, required: true

  field :order, Types::OrderType, null: true
  field :errors, [String], null: false

  def resolve(attributes:)
    order = Order.new(attributes.to_hash)
    order.total = order.customer.basket_total
    if order.save
      {
          order: order,
          errors: []
      }
    else
      {
          order: nil,
          errors: order.errors.full_messages
      }
    end
  end
end