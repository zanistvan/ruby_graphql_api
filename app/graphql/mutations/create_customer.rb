class Mutations::CreateCustomer < Mutations::BaseMutation
  argument :attributes, Types::CustomerAttributes, required: true

  field :customer, Types::CustomerType, null: true
  field :errors, [String], null: false

  def resolve(attributes:)
    customer = Customer.new(attributes.to_hash)
    if customer.save
      {
          customer: customer,
          errors: []
      }
    else
      {
          customer: nil,
          errors: customer.errors.full_messages
      }
    end
  end
end