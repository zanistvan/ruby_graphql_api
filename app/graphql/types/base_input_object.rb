module Types
  class BaseInputObject < GraphQL::Schema::InputObject
    argument_class Types::BaseArgument
  end

  class Types::CustomerAttributes < Types::BaseInputObject
    description "Attributes for creating or updating a customers"
    argument :name, String, required: true
    argument :surname, String, required: true
    argument :email, String, required: true
    argument :phone, String, required: true
    argument :gender, Integer, required: false
    argument :nationality, String, required: false
    argument :birthdate, GraphQL::Types::ISO8601Date, required: false
  end

  class Types::BasketAttributes < Types::BaseInputObject
    description "Attributes for creating baskets"
    argument :count, Integer, required: true
    argument :product_id, ID, required: true
    argument :customer_id, ID, required: true
  end

  class Types::OrderAttributes < Types::BaseInputObject
    argument :customer_id, ID, required: true
  end

  class Types::ProductAttributes < Types::BaseInputObject
    description "Attributes for creating or updating a product"
    argument :name, String, required: true
    argument :price, Float, required: true
    argument :stock, Integer, required: true
    argument :description, String, required: false
  end
end