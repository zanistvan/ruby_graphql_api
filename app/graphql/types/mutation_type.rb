module Types
  class MutationType < Types::BaseObject
    field :create_customer, mutation: Mutations::CreateCustomer
    field :create_basket, mutation: Mutations::CreateBasket
    field :create_order, mutation: Mutations::CreateOrder
    field :create_product, mutation: Mutations::CreateProduct

  end
end
