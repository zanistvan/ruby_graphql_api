module Types
  class OrderType < Types::BaseObject
    field :id, ID, null: false
    field :customer_id, ID, null: true
    field :products, GraphQL::Types::JSON, null: true
    field :total, Float, null: false
    field :is_success, Boolean, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
