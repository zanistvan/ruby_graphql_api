module Types
  class QueryType < Types::BaseObject
    field :all_customers, [Types::CustomerType], null: false
    field :all_baskets, [Types::BasketType], null: false
    field :all_products, [Types::ProductType], null: false
    field :all_orders, [Types::OrderType], null: false
    field :customer, Types::CustomerType, null: false do
      argument :id, ID, required: true
    end
    field :product, Types::ProductType, null: false do
      argument :id, ID, required: true
    end
    field :order, Types::OrderType, null: false do
      argument :id, ID, required: true
    end
    field :basket, Types::BasketType, null: false do
      argument :id, ID, required: true
    end
    field :customer_orders, [Types::OrderType], null: false do
      argument :customer_id, Integer, required: true
    end

    def customer_orders(customer_id:)
      Customer.find(customer_id).orders
    end

    def all_customers
      Customer.all
    end

    def all_orders
      Order.all
    end

    def customer(id:)
      Customer.find(id)
    end

    def all_products
      Product.all
    end

    def product(id:)
      Product.find(id)
    end

    def order(id:)
      Order.find(id)
    end

    def all_baskets
      Basket.all
    end

    def basket(id:)
      Basket.find(id)
    end
  end
end
