module Types
  class ProductType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: true
    field :price, Float, null: true
    field :stock, Integer, null: true
    field :description, String, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :baskets_count, Integer, null: false

    def baskets_count
      object.baskets.size
    end
  end
end
