module Types
  module Enums
    class Gender < Types::BaseEnum
      description 'All available genders'

      value("male", 0)
      value("female", 1)
      value("not_sure", 2)
      value("prefer_not_to_disclose", 3)
    end
  end
end

