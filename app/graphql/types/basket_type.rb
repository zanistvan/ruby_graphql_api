module Types
  class BasketType < Types::BaseObject
    field :id, ID, null: false
    field :customer_id, Integer, null: true
    field :product_id, Integer, null: true
    field :count, Integer, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false

    def baskets_count
      object.baskets.size
    end
  end
end
