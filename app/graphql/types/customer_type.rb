module Types
  class CustomerType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :surname, String, null: false
    field :email, String, null: false
    field :phone, String, null: false
    field :nationality, String, null: true
    field :gender, Enums::Gender, null: false
    field :birthdate, GraphQL::Types::ISO8601Date, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :baskets_count, Integer, null: false
    field :baskets, [Types::BasketType], null: false

    def baskets_count
      object.baskets.size
    end

    def orders_count
      object.orders.size
    end

  end
end
