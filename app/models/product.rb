class Product < ApplicationRecord
  has_many :baskets, dependent: :destroy

  validates :price, :stock, :presence => true, :numericality => { :greater_than_or_equal_to => 0}
  validates :name, :stock, :presence => true
end
