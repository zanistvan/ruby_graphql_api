class Basket < ApplicationRecord
  belongs_to :customer
  belongs_to :product
  validates :count, :presence => true, :numericality => { :greater_than => 0}

  before_validation do
    self.errors.add(:base, I18n.t('add_to_cart_exceed_stock', stock: self.product.stock)) if self.product.stock < self.count
  end


end
