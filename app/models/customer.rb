class Customer < ApplicationRecord
  enum gender: { male: 0, female: 1, not_sure: 2, prefer_not_to_disclose: 3 }

  has_many :orders, dependent: :destroy
  has_many :baskets, dependent: :destroy

  validates :name, :surname, :email, :phone, :presence => true
  validates :email, uniqueness: true

  def basket_total
    total=0
    baskets.each do |basket|
      total+=basket.product.price*basket.count
    end
    total
  end
end
