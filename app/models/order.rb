class Order < ApplicationRecord
  belongs_to :customer
  after_save :updater!

  before_validation do
    baskets = self.customer.baskets
    unless baskets.any?
      self.errors.add(:base, I18n.t('create_order_no_basket'))
    end
    baskets.each do |basket|
      product = basket.product
      if basket.count>product.stock
        self.errors.add(:base, I18n.t('order_stock_error', available: product.stock, product_name: product.name))
      end
    end
  end

  private
  def updater!
    baskets = self.customer.baskets
    products=[]
    baskets.each do |basket|
      product = basket.product
      product_hash={}
      product.update(stock: product.stock-basket.count)
      product_hash[:quantity]=basket.count
      product_hash[:name]=product.name
      product_hash[:price]=product.price
      product_hash[:pr_id]=product.id
      products.push(product_hash)
    end
    self.update_columns(is_success: true, products: products)
    baskets.destroy_all
  end
end
