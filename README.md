Basic API application in Ruby on Rails using GraphQL with RSpec tests
## Ruby GraphQL API Example
  - Ruby 2.6
  - Rails 5.2

## Models
  - Customer
  - Product
  - Basket
  - Order
### To run the project:
```ruby
rails db:create
```
```ruby
rails db:migrate
```
```ruby
rails db:seed
```
```ruby
rails s
```
### GraphQL Mutations Examples:

#### 1. Create a customer
```javascript
mutation {
  createCustomer(input: {
    attributes:{name: "customer_name",
    surname: "customer_surname",
    email: "customer_uniq_email",
    phone: "customer_phone",
    gender: customer_enum_gender_0_3,
    nationality: "customer_nationality",
    birthdate: "customer_birhdate"
    }
  }) {
    customer {
      id
      name
      surname
      email
      phone
      gender
      nationality
      birthdate
    }
    errors
  }
}
```
### 2. Create a product
```javascript
mutation {
      createProduct(input: {
        attributes:{name: "product_name",
        price: 10.12,
        stock: 10
        }
      }) {
        product {
          id
          name
          price
          stock
        }
        errors
      }
    }
```
### 3. Add product to basket
```javascript
mutation {
      createBasket(input: {
        attributes:{
          count: product_count,
          customerId: customer_id,
          productId: product_id
        }
      }) {
        basket {
          id
          count
          productId
          customerId
        }
        errors
      }
    }
```
### 4. Create order
```javascript
mutation {
      createOrder(input: {
        attributes:{
          customerId: customer_id,
        }
      }) {
        order {
          id
          total
          customerId
          products
        }
        errors
      }
    }
```
### Tests:
```ruby
bundle exec rspec spec/graphql/mutations/orders/order_spec.rb
```
```ruby
bundle exec rspec spec/graphql/mutations/baskets/basket_spec.rb
```
```ruby
bundle exec rspec spec/graphql/mutations/customers/customer_spec.rb
```
```ruby
bundle exec rspec spec/graphql/mutations/products/product_spec.rb
```
#### To run all tests
`bundle exec rake spec`
### Todos
 - Authentication
 - Pagination
 - Filtering