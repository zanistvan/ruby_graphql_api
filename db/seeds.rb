Customer.destroy_all
Product.destroy_all

20.times do
  Product.create(name: Faker::Commerce.product_name, price: Faker::Commerce.price, stock: Faker::Number.between(from: 10, to: 100), description: Faker::Lorem.sentence(word_count: 15))
end

10.times do
  customer = Customer.create(name: Faker::Name.first_name, surname: Faker::Name.last_name, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone_with_country_code, nationality: Faker::Nation.nationality, gender: Faker::Number.between(from: 0, to: 3))
  product = Product.find(Product.pluck(:id).sample)
  customer.baskets.create(product_id: product.id, count: Faker::Number.between(from: 1, to: product.stock))
end