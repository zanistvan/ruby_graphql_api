class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.references :customer, foreign_key: true
      t.json :products
      t.decimal :total
      t.boolean :is_success

      t.timestamps
    end
  end
end
