class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :surname
      t.string :email
      t.string :phone
      t.string :nationality
      t.integer :gender, default: 0, index: true
      t.date :birthdate

      t.timestamps
    end
  end
end
