FactoryBot.define do
  factory :product do
    sequence(:name) { |n| "Best Smart TV (#{n})" }
    price { 100 }
    stock { 99 }
  end
end

