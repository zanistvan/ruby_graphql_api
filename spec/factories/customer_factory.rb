FactoryBot.define do
  factory :customer do
    name { "John" }
    sequence(:surname) { |n| "Doe (#{n})" }
    sequence(:email) { |n| "#{DateTime.now.to_i}@example.com" }
    sequence(:phone) { |n| "+9054135682#{n}" }
    sequence(:birthdate) { 39.years.ago }
    gender {0}
  end
end
