require 'rails_helper'

module Mutations
  module Baskets
    RSpec.describe CreateBasket, type: :request do
      describe '.resolve' do
        let(:customer) { create(:customer) }
        let(:product) { create(:product) }
        it 'adds product to basket' do
          expect do
            post '/graphql', params: { query: add_to_basket(customer_id: customer.id, product_id: product.id, count: 1) }
          end.to change { customer.baskets.count }.by(1)
        end

        it 'should not add more than stock to basket' do
          post '/graphql', params: { query: add_to_basket(customer_id: customer.id, product_id: product.id, count: product.stock+1) }
          json = JSON.parse(response.body)
          data = json['data']['createBasket']

          expect(data).to include(
                              'basket'   => be_nil,
                              'errors'   => be_truthy
                          )
        end
      end

      def add_to_basket(customer_id:, product_id:, count:)
        <<~GQL
          mutation {
          createBasket(input: {
            attributes:{
              count: #{count},
              customerId: #{customer_id},
              productId: #{product_id}
            }
            
          }) {
            basket  {
                id
              }
            errors
          }
        }
        GQL
      end
    end
  end
end