require 'rails_helper'

module Mutations
  module Customers
    RSpec.describe CreateCustomer, type: :request do
      describe '.resolve' do
        it 'creates a customer' do
          expect do
            post '/graphql', params: { query: create_cus_query }
          end.to change { Customer.count }.by(1)
        end

        it 'returns a customer' do
          post '/graphql', params: { query: create_cus_query }
          json = JSON.parse(response.body)
          data = json['data']['createCustomer']['customer']

          expect(data).to include(
                              'id'              => be_present,
                              'name'              => 'John',
                              'surname'              => 'Doe',
                          )
        end
      end

      def create_cus_query
        <<~GQL
          mutation {
            createCustomer(input: {
              attributes:{
                name: "John",
                surname: "Doe",
                email: "#{DateTime.now.to_i}@email.com",
                phone: "#{Faker::PhoneNumber.cell_phone_with_country_code}",
                gender: 1,
                nationality: "#{Faker::Nation.nationality}",
              }
              
            }) {
              customer {
                id
                name
                surname
                email
                phone
                gender
                nationality
              }
              errors
            }
          }
        GQL
      end
    end
  end
end