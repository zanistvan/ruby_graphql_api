require 'rails_helper'

module Mutations
  module Orders
    RSpec.describe CreateOrder, type: :request do
      describe '.resolve' do
        let(:customer) { create(:customer) }
        let(:product) { create(:product) }

        it 'creates order' do
          basket = customer.baskets.create(product_id: product.id, count: product.stock)
          post '/graphql', params: { query: create_order(customer_id: customer.id) }
          json = JSON.parse(response.body)
          data = json['data']['createOrder']['order']

          expect(data).to include(
                              'id'      => be_present,
                              'customerId'      => customer.id.to_s,
                              'total'   => equal(basket.count*product.price.to_f),
                          )
          expect(product.reload.stock).to eq(0)
        end

        it 'creates order with multiple products' do
          product_2 = create(:product)
          customer.baskets.create(product_id: product.id, count: product.stock)
          customer.baskets.create(product_id: product_2.id, count: 1)
          total=0
          customer.baskets.each do |basket|
            total+=basket.product.price*basket.count
          end
          post '/graphql', params: { query: create_order(customer_id: customer.id) }
          json = JSON.parse(response.body)
          data = json['data']['createOrder']['order']
          products=[]
          customer.baskets.each do |basket|
            product = basket.product
            product_hash={}
            product_hash['quantity']=basket.count
            product_hash['name']=product.name
            product_hash['price']=product.price.to_s
            product_hash['pr_id']=product.id
            products.push(product_hash)
          end
          expect(data).to include(
                              'id'              => be_present,
                              'customerId'      => customer.id.to_s,
                              'total'           => equal(total.to_f)
                              )
          expect(data['products']).to match_array(products)
          expect(product.reload.stock).to eq(0)
          expect(product_2.stock-1).to eq(product_2.reload.stock)
        end

        it 'should not create order when enough stock is not available' do
          customer.baskets.create(product_id: product.id, count: product.stock+1)
          post '/graphql', params: { query: create_order(customer_id: customer.id) }
          json = JSON.parse(response.body)
          data = json['data']['createOrder']

          expect(data).to include(
                              'order'      => be_nil,
                              'errors'   => be_truthy
                              )
        end
      end

      def create_order(customer_id:)
        <<~GQL
          mutation {
            createOrder(input: {
              attributes:{
                customerId: #{customer_id},
              }
              
            }) {
              order {
                id
                total
                customerId
                products
              }
              errors
            }
          }
        GQL
      end
    end
  end
end