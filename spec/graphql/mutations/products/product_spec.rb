require 'rails_helper'

module Mutations
  module Products
    RSpec.describe CreateProduct, type: :request do
      describe '.resolve' do
        it 'creates a product' do
          expect do
            post '/graphql', params: { query: create_prdct_query }
          end.to change { Product.count }.by(1)
        end

        it 'returns a product' do
          post '/graphql', params: { query: create_prdct_query }
          json = JSON.parse(response.body)
          data = json['data']['createProduct']['product']

          expect(data).to include(
                              'id'   => be_present,
                              'name' => 'Smart Android TV',
                              'price'=> 199.99,
                          )
        end
      end

      def create_prdct_query
        <<~GQL
          mutation {
            createProduct(input: {
              attributes:{
                name: "Smart Android TV",
                price: 199.99,
                stock: 99
              }
              
            }) {
              product {
                id
                name
                price
                stock
              }
              errors
            }
          }
        GQL
      end
    end
  end
end